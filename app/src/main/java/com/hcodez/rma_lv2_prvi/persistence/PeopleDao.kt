package com.hcodez.rma_lv2_prvi.persistence

import androidx.room.*
import com.hcodez.rma_lv2_prvi.models.InspiringPerson

@Dao
interface PeopleDao {
    @Query("SELECT * FROM people")
    fun getPeople(): List<InspiringPerson>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(person: InspiringPerson)

    @Delete
    fun delete(person: InspiringPerson)

    @Query("SELECT * FROM people WHERE id=:id")
    fun getPeople(id: Long): InspiringPerson
}