package com.hcodez.rma_lv2_prvi.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hcodez.rma_lv2_prvi.models.InspiringPerson

@Database(entities = [InspiringPerson::class], version = 1)
abstract class PeopleDatabase : RoomDatabase() {
    abstract fun peopleDao(): PeopleDao

    companion object {
        const val NAME = "peopleDb"
    }
}