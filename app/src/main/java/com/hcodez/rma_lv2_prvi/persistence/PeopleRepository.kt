package com.hcodez.rma_lv2_prvi.persistence

import com.hcodez.rma_lv2_prvi.models.InspiringPerson
import com.hcodez.rma_lv2_prvi.utilities.getScientistDescription
import kotlin.random.Random

object PeopleRepository {
    private val scientists = mutableListOf<InspiringPerson>(
        InspiringPerson(
            0,
            "Steve Wozniak",
            "Steve Wozniak",
            "11. kolovoza 1950.",
            getScientistDescription("Steve Wozniak")
        ),
        InspiringPerson(
            0,
            "Elon Musk",
            "Elon Musk",
            "June 28, 1971",
            getScientistDescription("Elon Musk")
        ),
        InspiringPerson(
            0,
            "Bill Gates",
            "Bill Gates",
            "28. listopada 1955.",
            getScientistDescription("Bill Gates")
        ),
        InspiringPerson(
            0,
            "Tim Berners-Lee",
            "Tim Berners-Lee",
            "June 8, 1955",
            getScientistDescription("Tim Berners-Lee")
        ),
    )

    fun getPeople(): List<InspiringPerson> = scientists
    fun insert(scientist: InspiringPerson) = scientists.add(scientist)

    private val scientistQuotes = mutableMapOf<String, MutableList<String>>(
        "Steve Wozniak" to mutableListOf<String>(
            "Never trust a computer you can't throw out a window.",
            "Wherever smart people work, doors are unlocked.",
            "Atari is a very sad story.",
        ),
        "Elon Musk" to mutableListOf<String>(
            "You miss 100% of the shots you don't take.",
            "Your most unhappy customers are your greatest source of learning.",
            "Life is not fair; get used to it.",
        ),
        "Bill Gates" to mutableListOf<String>(
            "Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
            "Whether you think you can or you think you can't, you're right.",
            "I have learned over the years that when one's mind is made up, this diminishes fear.",
        ),
        "Tim Berners-Lee" to mutableListOf<String>(
            "Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
            "Whether you think you can or you think you can't, you're right.",
            "I have learned over the years that when one's mind is made up, this diminishes fear.",
        ),
    )

    fun insertQuote(name: String, quote: String) {
        if (quote.isNotBlank()) {
            if (!scientistQuotes.containsKey(name))
                scientistQuotes.put(name, mutableListOf<String>(quote))
        }
    }

    fun getRandomQuoteFrom(name: String): String? {
        val randQuoteNumber = scientistQuotes[name]?.let { Random.nextInt(it.size) }
        return randQuoteNumber?.let { scientistQuotes[name]?.get(it) }
    }
}