package com.hcodez.rma_lv2_prvi.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.hcodez.rma_lv2_prvi.persistence.PeopleRepository
import com.hcodez.rma_lv2_prvi.R
import com.hcodez.rma_lv2_prvi.adapters.InspiringPersonAdapter
import com.hcodez.rma_lv2_prvi.databinding.ActivityMainBinding
import com.hcodez.rma_lv2_prvi.persistence.PeopleDao
import com.hcodez.rma_lv2_prvi.persistence.PeopleDatabase
import com.hcodez.rma_lv2_prvi.persistence.PeopleDatabaseBuilder

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: InspiringPersonAdapter
    private val peopleRepository: PeopleDao by lazy {
        PeopleDatabaseBuilder.getInstance().peopleDao()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        initializeUI()
    }

    private fun initializeUI() {
        initializeRecyclerView()

        mainBinding.fabAddNewInspiringPerson.setOnClickListener {
            val newPersonIntent = Intent(this, NewPersonActivity::class.java)
            startActivity(newPersonIntent)
        }
    }

    private fun initializeRecyclerView() {
        linearLayoutManager = LinearLayoutManager(this)
        mainBinding.recyclerView.layoutManager = linearLayoutManager

        //podaci iz LV2 zadatka su koristeni cisto kao neki podaci s cime cu napuniti bazu da ne bude u pocetku prazna
        if (peopleRepository.getPeople().isEmpty()) {
            for (person in PeopleRepository.getPeople()) {
                peopleRepository.insert(person)
            }
        }

        var scientists = peopleRepository.getPeople()
        adapter = InspiringPersonAdapter(scientists)
        mainBinding.recyclerView.adapter = adapter
    }

    override fun onResume() {
        adapter.refreshData(peopleRepository.getPeople())
        super.onResume()
    }
}