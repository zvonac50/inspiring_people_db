package com.hcodez.rma_lv2_prvi.utilities

import com.hcodez.rma_lv2_prvi.R

fun getColorResource(image: String): Int {
    return when (image) {
        "Steve Wozniak" -> R.drawable.steve_wozniak
        "Elon Musk" -> R.drawable.elon_musk
        "Bill Gates" -> R.drawable.bill_gates
        "Tim Berners-Lee" -> R.drawable.tbl
        else -> R.drawable.unknown_scientist
    }
}

fun getScientistDescription(name: String): String {
    return when (name) {
        "Steve Wozniak" -> "Amerikanac je poljskog podrijetla, računalni znanstvenik i suosnivač računalne tvrtke Apple Computers Inc. zajedno sa Steveom Jobsom i Ronaldom Waynom. Njegovi izumi doprinijeli su razvoju osobnih računala tijekom sedamdesetih godina prošlog stoljeća."
        "Elon Musk" -> "Osim što je jedan od najbogatijih, najinovativnijih, najpametnijih ljudi na ovom planetu, on je možda, po mom mišljenju, jedina osoba koja se ne osvrće na prošlost i zaista želi unaprijediti i poboljšati naše živote."
        "Bill Gates" -> "Smatra se poduzetnikom, liderom, vizionarom, investitorom, poslovnim magnatom, filantropom, misliocem i, ono najvažnije, suosnivačem Microsofta. Taj računalni genij obilježio je 21. stoljeće u razvoju računala."
        "Tim Berners-Lee" -> "1989. godine, izumio World Wide Web, internetsku hipermedijsku inicijativu za globalnu razmjenu informacija dok je bio u CERN-u, Europskom laboratoriju za fiziku čestica."
        else -> "Unknown description for unknown scientist"
    }
}

/*
fun getScientistQuote(name: String): String {
    return when (name) {
        "Steve Wozniak" ->
        "Elon Musk" ->
        "Bill Gates" ->
        "Tim Berners-Lee" ->
    }
}
*/